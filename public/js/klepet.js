var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var vzdevekZasebno = besede[0].substring(1,besede[0].length-1);
      besede.shift();
      besede[0] = besede[0].substring(1,besede[0].length);
      besede[besede.length-1] =  besede[besede.length-1].substring(0, besede[besede.length-1].length-1 );
      besede = besede.join(' ');
      this.socket.emit('zasebnoSporociloZahteva',vzdevekZasebno, besede);
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};

Klepet.prototype.procesirajSmeskote = function(ukaz) {

  ukaz = ukaz.replace( /;\)/g ,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png "/>');
  ukaz = ukaz.replace( /:\)/g ,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png "/>');
  ukaz = ukaz.replace( /\(y\)/g ,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png "/>');
  ukaz = ukaz.replace( /:\*/g ,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png "/>');
  ukaz = ukaz.replace( /:\(/g ,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png "/>');
  return ukaz;
};