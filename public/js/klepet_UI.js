function divElementEnostavniTekst(sporocilo) {
  if(sporocilo.match(/img src="https:\/\/dl.dropboxusercontent.com\/u\/2855959\/moodle\/common\/emoticons.*"/g))
    return $('<div style="font-weight: bold"></div>').html(sporocilo);
  else
    return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;
  
  sporocilo = klepetApp.procesirajSmeskote(sporocilo);
  
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    sporocilo = filtrirajVulgarnebesede(sporocilo);
    var kanal = $('#kanal').text();
    kanal = kanal.substring(kanal.indexOf("@")+2,kanal.length);
    klepetApp.posljiSporocilo(kanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Filtriranje fulgarnih besed
var path = '/swearWords.txt';
var vulgarneBesede = [];

function naloziBesde (){
   $.ajax({
     type: 'GET',
     url: path,
     async: false,
     success: function(besede) {
          vulgarneBesede = besede.split('\n').map(function(dela){ return dela.trim(); });
     }
   });
}

function filtrirajVulgarnebesede (sporocilo){
  for(var i in vulgarneBesede){
    if(sporocilo.indexOf(vulgarneBesede[i])>=0){  
        var prepovedano = vulgarneBesede[i].replace(/./g, '*');
        for(;sporocilo.indexOf(vulgarneBesede[i]) >= 0;)
          sporocilo = sporocilo.replace(vulgarneBesede[i], prepovedano);
      }
    }
  return sporocilo;
}

var socket = io.connect();

$(document).ready(function() {
  
  var klepetApp = new Klepet(socket);
  naloziBesde(); // nalaganje besed v pomnilnik

 socket.on('zasebnoSporociloOdgovor', function(rezultat){
    $('#sporocila').append(divElementHtmlTekst(rezultat.sporocilo));
 })

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      // prikazovanje imena 
      var tmp = $('#kanal').text();
      tmp = tmp.substring(tmp.indexOf("@")+1);
      $('#kanal').text(rezultat.vzdevek + " @ " + tmp);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    //prikazovanje kanala
    var tmp1 = $('#kanal').text();
    tmp1 = tmp1.substring(0,tmp1.indexOf("@") +1);
    $('#kanal').text(tmp1 +" "+  rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = "";
    if(sporocilo.besedilo.match(/img src="https:\/\/dl.dropboxusercontent.com\/u\/2855959\/moodle\/common\/emoticons.*"/g))
      novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    else
      novElement = $('<div style="font-weight: bold"></div>').text(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki');
  }, 1000);
  
  socket.on('uporabniki',function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    
    for(var up in uporabniki){
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[up]));
    }
  });

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});